import xnat
import csv
import logging
import os
from typing import List
from tqdm import tqdm
from xnat.session import XNATSession
from xnat.core import XNATBaseObject

logger = logging.getLogger(__name__)


class XNATParserError(ValueError):
    """Exception that can contain an list of errors from XNAT.

    Parameters
    ----------
    message: str
        Exception message
    error_list: list
        List of strings containing error messages. Default is None

    """

    def __init__(self, message: str, error_list: List[str] = None):
        super().__init__(message)
        self.error_list = error_list

def get_exp_ohifurl (experiment: XNATBaseObject, project_id:str) -> str:
    """ constructs the ohif viewer url of a subject

    Parameters
    ----------
    subject: XNATBaseObject
        An subject instance of the XNAT instance
    project_id : str
        The project id of the subject

    Returns
    -------
    Str
        The url of the xnat ohif viewer for that experiment

    """
    host = subject.external_uri().split('/data/')[0]
    url = f'{host}/VIEWER/?subjectId={subject.id}\
            &projectId={project_id}&experimentId={experiment.id}\
            &experimentLabel={experiment.label}'
    
    return url

def get_sub_ohifurl (subject: XNATBaseObject, project_id:str) -> str:
    """ constructs the ohif viewer url of a subject

    Parameters
    ----------
    subject: XNATBaseObject
        An subject instance of the XNAT instance
    project_id : str
        The project id of the subject

    Returns
    -------
    Str
        The url of the xnat ohif viewer for that subject

    """
    host = subject.external_uri().split('/data/')[0]
    url = f'{host}/VIEWER/?subjectId={subject.id}&projectId={project_id}'
    
    return url

def xnat_to_cbio(session: XNATSession, project: str, level: str, output: str, format: str, resource_id:str):
    """Acquires a list of subject/experiment IDs and OHIF viewer urls by iterating the xnat project

    Parameters
    ----------
    session : XNATSession
        An XNATSession of the XNAT instance that is going to be queried
    project : str
        The name of project desired to be linked to cbio
    level : str
        The level of linkage desired, subject or experiment 
    resource_id: str
        Name of the resource to be refered in cbioportal
    output: str
        Desired location/path for file

    Returns
    -------

    """
    url_lists = []
    
    try:
        xnat_project = session.projects[project]
    except XNATParserError as v:
        logger.error(f"Project {project} could not be read: {v}")

    for s in tqdm(xnat_project.subjects.values()):
        if level == 'sub':
            try:
                url = get_sub_ohifurl(s, xnat_project.id)
                url_lists.append([s.label, resource_id, url])

            except XNATParserError as v:
                logger.warning(f"Url of subject {s.label} could not be extracted: {v}")
        elif level == 'exp':

            for e in tqdm(session.xnat_project.subjects.values()):
                try:
                    url = get_exp_ohifurl(e, xnat_project.id)
                    url_lists.append([e.label,resource_id,url])

                except XNATParserError as v:
                    logger.warning(f"Url of experiment {e.label} could not be extracted: {v}")
        else:
            logger.error(f"Unrecognized level {level}")

    #return url_lists
    write_file(url_lists, output, format)
    logger.info(f" File for cbioPortal is written in {output}.")

def write_file(xnat_data: List, output: str, format: str):
    """ writes a text file including all revevant data for linking in cbio-portal

    Parameters
    ----------
    xnat_data : List
        An List containing Subject ids, cbio tab identifier name and access urls

    Returns
    -------

    """
    fields = ["PATIENT_ID", "RESOURCE_ID", "URL"]

    if not os.path.exists(output):
        os.makedirs(output)
        
    with open(output + '/xnat-cbio.' + format, 'w') as f:
        writer = csv.writer(f, delimiter =' ')
        writer.writerow(fields)    
        writer.writerows(xnat_data)
