import logging
import click
import xnat
from pathlib import Path
from xnat2cbio.xnat_query import xnat_to_cbio

logger = logging.getLogger(__name__)

def __connect_xnat(server: str, username, password):
    """This function collects credentials and connects to XNAT

    Parameters
    ----------
    server : str
        XNAT server to connect (including https://)
    args : Namespace
        Namespace containing commandline arguments

    Returns
    -------
    XNATSession
    """

    logger.debug(f"Connecting to {server}")
    try:
        session = xnat.connect(server=server, user=username, password=password)
    except:
        logger.error(f"{username} could not connect to {server}.")
    return session

@click.option("-v", "--verbose", is_flag=True, default=False, help="Enables debugging mode.")
@click.option(
    "-i",
    "--ResourceId",
    type=str,
    default='XNAT-OHIF',
    help=(
        "Resource identifier to be used in cbioportal. default is 'XNAT-OHIF'. "
    ),
)
@click.option(
    "-f",
    "--format",
    default="txt",
    type=click.Choice(
        ["txt", "csv"], case_sensitive=False
    ),
    help=(
        "File format of output file. Defaults to txt."
    ),
)
@click.option(
    "-o",
    "--output",
    required=True,
    type=click.Path(writable=True, dir_okay=True, file_okay=False),
    default="./output/",
    help=(
        "Destination path to write output to. If not set,"
        "output will be written in the 'output' folder of the current directory."
    ),
)
@click.option(
    "--level",
    default="sub",
    type=click.Choice(
        ["sub", "exp"], case_sensitive=False
    ),
    help=(
        "The level of info needed for linkage, subject or experiment. Defaults to subject."
    ),
)
@click.option(
    "--project",
    type=str,
    required=True,
    default=None,
    help=(
        "Name of the project on XNAT to be linked with cbioportal."
    ),
)
@click.option(
    "-p",
    "--password",
    type=str,
    default=None,
    help=(
        "Password to use with the username."
    ),
)
@click.option(
    "-u",
    "--username",
    type=str,
    default=None,
    help=f"Username to use, leave empty to use netrc.",
)
@click.option(
    "-s",
    "--host",
    type=str,
    required=True,
    help=f"URI of XNAT host/server to connect to (including http:// or https://).",
)
@click.command()
def cli(
    host: str,
    username: str,
    password: str,
    project: str,
    level: str,
    resourceid: str,
    output: click.Path,
    format: str,
    verbose: bool):
    """ This tool generates a file used to link an XNAT project to cbioportal."""

    # Set logging level
    logger = logging.getLogger(__name__)
    logger.info("======= xnat2cbio ========")
    if verbose:
        logger.setLevel(logging.DEBUG)
        logger.debug("Verbose mode enabled")

    # Connect to XNAT
    if username is None or password is None:
        try:
            session = __connect_xnat(host, username, password)
        except Exception as e:
            logger.error(f'Trouble logging into {host}: {e}')
    else:
        try:
            session = __connect_xnat(host)
        except Exception as e:
            logger.error(f'Trouble logging into {host}: {e}')

    # Genrate file from query
    xnat_to_cbio(session, project, level, output, format, resourceid)