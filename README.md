$ xnat2cbio

  xnat2cbio is a simple tool that generates  
  a file used to link an XNAT project to cbioportal. <br/>
  It queries the contents of an xnat project 
  to produce the file.

Installation:

```
 $ pip install .
```
Manual:
  ```
  $ xnat2cbio --help
  
  Usage: xnat2cbio [OPTIONS]

    This tool generates a file used to link an XNAT project to cbioportal.

  Options:
    -s, --host TEXT         URI of XNAT host/server to connect to (including
                            http:// or https://).  [required]
    -u, --username TEXT     Username to use, leave empty to use netrc.
    -p, --password TEXT     Password to use with the username.
    --project TEXT          Name of the project on XNAT to be linked with
                            cbioportal.  [required]
    --level [sub|exp]       The level of info needed for linkage, subject or
                            experiment. Defaults to subject.
    -o, --output DIRECTORY  Destination path to write output to. If not
                            set,output will be written in the 'output' folder of
                            the current directory.  [required]
    -f, --format [txt|csv]  File format of output file. Defaults to txt.
    -i, --ResourceId TEXT   Resource identifier to be used in cbioportal.
                            default is 'XNAT-OHIF'.
    -v, --verbose           Enables debugging mode.
    --help                  Show this message and exit.
  ```

Example usage:

  ```
  $ xnat2cbio -s 'https://xnat.bmia.nl/' --project 'EOSC4Cancer_TCGA_COAD'
  ```